/**
 * Вид экрана после смерти игрока
 */
export class GameOverScreen {
    words: any;
    colors: any;
    id: any;

    constructor() {
        this.words = ['GAME OVER','WASTED', 'YOU DIED'];
        this.colors = ['#f80000', '#ffffff', '#a9203e'];
        this.id = 'text';
        (document.getElementById('dead-screen') as HTMLElement).classList.add('show');
        this.showGameOverScreen(this.words, this.colors);
    }

    showGameOverScreen(words: any, colors: any) {
        if (colors === undefined) colors = ['#fff'];
        let visible = true;
        let con = document.getElementById('console');
        let letterCount = 1;
        let x = 1;
        let waiting = false;
        let target = document.getElementById(this.id)
        target.setAttribute('style', 'color:' + this.colors[0])
        window.setInterval(function () {

            if (letterCount == 0 && waiting == false) {
                waiting = true;
                target.innerHTML = words[0].substring(0, letterCount)
                window.setTimeout(function () {
                    let usedColor = colors.shift();
                    colors.push(usedColor);
                    let usedWord = words.shift();
                    words.push(usedWord);
                    x = 1;
                    target.setAttribute('style', 'color:' + colors[0])
                    letterCount += x;
                    waiting = false;
                }, 1300)
            } else if ((letterCount == words[0].length + 1) && waiting == false) {
                waiting = true;
                window.setTimeout(function () {
                    x = -1;
                    letterCount += x;
                    waiting = false;
                }, 1000)
            } else if (waiting == false) {
                target.innerHTML = words[0].substring(0, letterCount)
                letterCount += x;
            }
        }, 120)
        window.setInterval(function () {
            if (visible == true) {
                con.className = 'console-underscore hidden'
                visible = false;

            } else {
                con.className = 'console-underscore'

                visible = true;
            }
        }, 500)
    }
}